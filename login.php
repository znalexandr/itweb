
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Авторизация</title>

    <!-- Bootstrap -->
    <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="template/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">     
            <form method="POST" action="index.php">
              <h1>Авторизация</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required=""  name="login"/>
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" name="pass" />
              </div>
              <div>
                <button class="btn btn-default submit">Вход</button>
              </div>
              <div class="clearfix"></div>
            </form>
          </section>
        </div>
      </div>
  </body>
</html>
