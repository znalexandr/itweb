$(function(){
	$.ajaxSetup({
  		beforeSend: function(){NProgress.start();},
        complete: function(){NProgress.done();}
	});

	$(document).on("click", "[data-remove-item]", function(e){
        e.preventDefault();
        var isValid = true;
	    var confrim = "";
	    var url = "";
	    var redirect = "";
	    var item_id = $(this).data("item-id");
	    var type = $(this).data("remove-item");
	    switch(type){
	    	case('project'):
	    		url = "index.php?ajax=project_remove";
	    		redirect = "index.php?view=projects";
          confrim = "Вы уверены что хотите удалить проект?"
	    	break;
	    	case('task'):
	    		url = "index.php?ajax=task_remove";
	    		redirect = "index.php?view=tasks";
          confrim = "Вы уверены что хотите удалить задачу?"
	    	break;
        case('worker'):
          url = "index.php?ajax=worker_remove";
          redirect = "index.php?view=workers";
          confrim = "Вы уверены что хотите удалить сотрудника?"
        break;
	    }

        var isDelete = confirm(confrim);
        if(item_id == undefined){
        	isValid = false;
        	console.log("id is not defined");
        } else if(!isDelete) {
        	isValid = false;
        	console.log("id not delete");
        }
        
        if(isValid){
          $.ajax({
              url: url,
              method: 'POST',
              data: {item_id: item_id},
              success: function(res){
                if(res != "false"){
                	//console.log(res);
                	document.location.href = redirect;
                } else {
                	console.log(res);
                }
              }
          });
        }
    });

});