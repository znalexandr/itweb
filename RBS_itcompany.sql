-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 18 2017 г., 00:04
-- Версия сервера: 5.5.53
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `RBS_itcompany`
--

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE `projects` (
  `id` int(15) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`) VALUES
(10, 'Разработка сайта для ООО Агрохран', 'Даsлеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты. Букв то последний запятых взгляд семантика, рекламных назад рукописи буквоград свой дорогу, вдали это большого он заманивший текст. Предупредила рукописи возвращайся до эта это оксмокс всеми заглавных встретил жизни снова сих грустный послушавшись они за лучше, ее всемогущая о дорогу запятой? Решила вопрос свою текст составитель лучше правилами речью путь на берегу рот он переписывается, буквенных одна знаках назад великий злых, предложения продолжил большого, sимени прямо дал жизни жаренные однажды. Семантика последний взгляд курсивных имени, себя над грустный родного пустился? Взгляд осталось домах курсивных которой составитель коварный знаках свой необходимыми, от всех.'),
(11, 'Разработка мобильного приложения для ИП Микронов ', 'textarea has a value (JavaScript) property just like a text input. It doesn\'t have an HTML value attribute, but that doesn\'t matter, because both val() and attr(\'value\') write to the JavaScript value property. Note, attr() does not set HTML attributes! It only sets JavaScript properties, whilst trying to hide the difference for cases like class/className where the name is different. But you will still see the difference for places where the attribute and property do different things, such as in form elements. – bobince Sep 8 ');

-- --------------------------------------------------------

--
-- Структура таблицы `project_workers`
--

CREATE TABLE `project_workers` (
  `id` int(15) NOT NULL,
  `project_id` int(15) NOT NULL,
  `worker_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `project_workers`
--

INSERT INTO `project_workers` (`id`, `project_id`, `worker_id`) VALUES
(28, 10, 1),
(29, 10, 13),
(30, 10, 7),
(31, 10, 22),
(47, 11, 1),
(48, 11, 23),
(49, 11, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE `tasks` (
  `id` int(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `task_anons` tinytext NOT NULL,
  `task_desc` text NOT NULL,
  `worker` int(15) NOT NULL,
  `deadline` datetime NOT NULL,
  `added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `task_anons`, `task_desc`, `worker`, `deadline`, `added`) VALUES
(1, 'Первая задача', 'Небольшое описание задачи', 'Подробное описание', 1, '2017-02-22 00:00:00', '2017-02-16 08:33:00'),
(3, 'Тестовая задача', 'Краткое описание \\eeeee', 'asdasd', 1, '2017-02-22 00:00:00', '2017-02-16 18:17:52'),
(4, 'Новые заголовки для ООО \"Саратов Строй\"', 'Подготовить новые заголовки для главной страницы', '', 21, '2017-02-17 14:00:00', '2017-02-16 18:19:00'),
(5, 'Разработать логотип для ПАО \"Сбербанк России\" ', 'Разработка логотипа', '', 19, '2017-02-17 00:00:00', '2017-02-16 18:21:33'),
(8, 'Разработка приложения', 'ффффф', '', 23, '2017-02-18 00:00:00', '2017-02-17 23:37:59');

-- --------------------------------------------------------

--
-- Структура таблицы `workers`
--

CREATE TABLE `workers` (
  `id` int(15) UNSIGNED NOT NULL,
  `fio` varchar(50) NOT NULL,
  `position` tinyint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `workers`
--

INSERT INTO `workers` (`id`, `fio`, `position`) VALUES
(1, 'Иванов Иван Иванович', 1),
(2, 'Суханова Татьяна Кузьмевна', 2),
(3, 'Ерзов Петр Елизарович', 2),
(4, 'Шелепина Ариадна Михеевна', 2),
(5, 'Ярославов Терентий Эмилевич', 2),
(6, 'Кузинкова Лариса Георгиевна', 2),
(7, 'Лукьянов Викентий Евгениевич', 2),
(8, 'Балабанова Доминика Геннадиевна', 2),
(9, 'Сурков Захар Никанорович', 2),
(10, 'Зимнякова Маргарита Федоровна', 2),
(11, 'Пыстогов Елисей Сидорович', 2),
(12, 'Васенин Демьян Кириллович', 2),
(13, 'Сёмина Инесса Данииловна', 2),
(15, 'Петрухина Лидия Ивановна', 3),
(16, 'Поникаров Ким Левович', 3),
(17, 'Кая Герман Андронович', 3),
(18, 'Шеншина Вера Ивановна', 3),
(19, 'Антонова Виктория Фомевна', 3),
(20, 'Циглер Клавдий Игнатиевич', 4),
(21, 'Сальков Григорий Касьянович', 4),
(22, 'Мартынов Сергей Александрович ', 4),
(23, 'Зинин Александр Сергеевич', 1),
(24, 'Михалков Алексей Сергеевич', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `worker_position`
--

CREATE TABLE `worker_position` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `worker_position`
--

INSERT INTO `worker_position` (`id`, `name`) VALUES
(1, 'Гл. Инженер'),
(2, 'Веб-разработчик'),
(3, 'Веб-дизайнер'),
(4, 'Копирайте');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `project_workers`
--
ALTER TABLE `project_workers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `workers`
--
ALTER TABLE `workers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `worker_position`
--
ALTER TABLE `worker_position`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `project_workers`
--
ALTER TABLE `project_workers`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `workers`
--
ALTER TABLE `workers`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `worker_position`
--
ALTER TABLE `worker_position`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
