<?php

defined('ZNALEXANDR') or die('Access denied');

/* ===Распечатка массива=== */
function print_arr($arr){
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}
/* ===Распечатка массива=== */

/* ===Фильтрация входящих данных=== */
function clear($var){
    $var = mysql_real_escape_string(strip_tags($var));
    return $var;
}
/* ===Фильтрация входящих данных=== */

/* ===Редирект=== */
function redirect(){
    $redirect = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : PATH;
    header("Location: $redirect");
    exit;
}
/* ===Редирект=== */

/* ===Выход пользователя=== */
function logout(){
    unset($_SESSION['auth']);
}
/* ===Выход пользователя=== */



/* ===Постраничная навигация=== */
function pagination($page, $pages_count){
    if($_SERVER['QUERY_STRING']){ // если есть параметры в запросе
        foreach($_GET as $key => $value){
            // формируем строку параметров без номера страницы... номер передается параметром функции
           if($key != 'page') $uri .= "{$key}={$value}&amp;";
        }  
    }
    
    // формирование ссылок
    $back = ''; // ссылка НАЗАД
    $forward = ''; // ссылка ВПЕРЕД
    $startpage = ''; // ссылка В НАЧАЛО
    $endpage = ''; // ссылка В КОНЕЦ
    $page2left = ''; // вторая страница слева
    $page1left = ''; // первая страница слева
    $page2right = ''; // вторая страница справа
    $page1right = ''; // первая страница справа
    
    if($page > 1){
        $back = "<a class='nav_link' href='?{$uri}page=" .($page-1). "'>&lt;</a>";
    }
    if($page < $pages_count){
        $forward = "<a class='nav_link' href='?{$uri}page=" .($page+1). "'>&gt;</a>";
    }
    if($page > 3){
        $startpage = "<a class='nav_link' href='?{$uri}page=1'>&laquo;</a>";
    }
    if($page < ($pages_count - 2)){
        $endpage = "<a class='nav_link' href='?{$uri}page={$pages_count}'>&raquo;</a>";
    }
    if($page - 2 > 0){
        $page2left = "<a class='nav_link' href='?{$uri}page=" .($page-2). "'>" .($page-2). "</a>";
    }
    if($page - 1 > 0){
        $page1left = "<a class='nav_link' href='?{$uri}page=" .($page-1). "'>" .($page-1). "</a>";
    }
    if($page + 2 <= $pages_count){
        $page2right = "<a class='nav_link' href='?{$uri}page=" .($page+2). "'>" .($page+2). "</a>";
    }
    if($page + 1 <= $pages_count){
        $page1right = "<a class='nav_link' href='?{$uri}page=" .($page+1). "'>" .($page+1). "</a>";
    }
    
    // формируем вывод навигации
    echo '<div class="pagination">' .$startpage.$back.$page2left.$page1left.'<a class="nav_active">'.$page.'</a>'.$page1right.$page2right.$forward.$endpage. '</div>';
}
/* ===Постраничная навигация=== */





function dateFun($value, $vreme = true){
  $montharray = array( "01" => "Января", "02" => "Февраля", "03" => "Марта","04" => "Апреля","05" => "Мая","06" => "Июня","07" => "Июля","08" => "Августа","09" => "Сентября","10" => "Октября","11" => "Ноября","12" => "Декабря");
  $time = explode(' ',$value);
  $date = $time[0];
  $dateconvert = explode('-',$date);
  $year  = $dateconvert[0];
  $month = $montharray [$dateconvert[1]];
  $day   = $dateconvert[2];
  $time = $time[1];
  if($vreme == true) {
   $mydate = $day." ".$month." ".$year." в ".$time;
  } else {
   $mydate = $day." ".$month." ".$year;
  }
 return $mydate;
} 

function plural_text($i, $one, $two, $many){ 
     
     if ($i%10==1 && $i%100!=11){ $i= 0; } 
     elseif($i%10>=2 && $i%10<=4 && ($i%100<10 || $i%100>=20)) { $i=1; } else { $i=2; }      
      
     switch ($i) { 
         case 0:  $plural = $one; break; 
         case 1:  $plural = $two; break; 
         default: $plural = $many; break; 
     } 
return $plural; 
}  
















