<?php

defined('ZNALEXANDR') or die('Access denied');


/* === Все проекты === */
function projects($project_id = false){

    $predicat = "";
    if($project_id){
        $predicat = "WHERE projects.id = $project_id";
    }
    $project_query = "SELECT projects.id AS project_id, 
                     projects.name AS project_name, 
                     projects.description AS project_desc
                        FROM projects
                        ".$predicat."
                        ORDER BY projects.id DESC";

    $workers_query = "SELECT projects.id AS project_id, 
                             workers.fio AS worker_fio, 
                             workers.id AS worker_id,
                             workers.position AS worker_position_id,
                             worker_position.name AS worker_position
                                FROM projects 
                                    LEFT JOIN project_workers ON project_workers.project_id = projects.id 
                                    LEFT JOIN workers ON workers.id = project_workers.worker_id 
                                    LEFT JOIN worker_position ON workers.position = worker_position.id 
                                    ".$predicat."
                                        ORDER BY worker_position_id ASC";

    $project_res = mysql_query($project_query) or die(mysql_error());
    $workers_res = mysql_query($workers_query) or die(mysql_error());
    
    $projects = array();
    
    while($project_row = mysql_fetch_assoc($project_res)){
            $projects[$project_row["project_id"]] = $project_row;
    }   

    while($workers = mysql_fetch_assoc($workers_res)){
        $projects[$workers["project_id"]]["workers"][] = $workers;
    }

    return $projects;
}
/* === Все проекты === */

/* === Все сотрудники === */

function workers($worker_id = false, $modify = false){
    
    $predicat = "";
    $predicat2 = "";
    if($worker_id && $modify == false){
        $predicat = "WHERE workers.id = ".$worker_id;
    } else if($modify == "full" && $worker_id){
        $predicat = "WHERE workers.id = ".$worker_id;
        $predicat2 = "AND workers.id = ".$worker_id;
    }


    $query = "SELECT workers.id, workers.fio, workers.position AS position_id, worker_position.name AS position
                    FROM workers 
                        LEFT JOIN worker_position ON workers.position = worker_position.id 
                         $predicat
                               ORDER BY workers.position ASC";
    $res = mysql_query($query) or die(mysql_error());
    
    $workers = array();
    while($row = mysql_fetch_assoc($res)){
        $workers[$row['id']] = $row;
    }

    if($modify == "full"){
        $tasks_sql = "SELECT workers.id AS worker_id, 
                             tasks.id AS task_id,
                             tasks.name AS task_name,
                             tasks.task_anons,
                             tasks.added AS task_added,
                             tasks.deadline AS task_deadline
                                FROM workers
                                    LEFT JOIN tasks ON workers.id = tasks.worker
                                        WHERE tasks.id > 0 ".$predicat2."
                                            ORDER BY tasks.id ASC";
        $tasks_res = mysql_query($tasks_sql) or die(mysql_error());

        while($tasks_row = mysql_fetch_assoc($tasks_res)){
            $workers[$tasks_row['worker_id']]['tasks'][] = $tasks_row;
            $workers[$tasks_row['worker_id']]['tasks_count'] = count($workers[$tasks_row['worker_id']]['tasks']);
        }

        $project_sql = "SELECT workers.id AS worker_id,  
                               projects.id AS projects_id,
                               projects.name AS projects_name,
                               projects.description AS projects_description
                               FROM workers
                                    LEFT JOIN project_workers ON workers.id = project_workers.worker_id
                                    LEFT JOIN projects ON project_workers.project_id = projects.id
                                        WHERE projects.id > 0 ".$predicat2."
                                        ORDER BY projects.id ASC";
        $project_res = mysql_query($project_sql) or die(mysql_error());

        while($project_row = mysql_fetch_assoc($project_res)){
            $workers[$project_row['worker_id']]['projects'][] = $project_row;
            $workers[$project_row['worker_id']]['projects_count'] = count($workers[$project_row['worker_id']]['projects']);
        }     
    }

    return $workers;
}
/* === Все сотрудники === */

/* === Добавить проект === */
function project_add(){
    $name = clear($_POST['name']);
    $desc = clear($_POST['desc']);
    $workers = $_POST['workers'];

    $project_query = "INSERT INTO projects (name, description) VALUES ('$name', '$desc')";
    $project_res = mysql_query($project_query) or die(mysql_error());

    $team_query = "INSERT INTO project_workers (project_id, worker_id) VALUES ";

    if(mysql_affected_rows() > 0){
        $project_id = mysql_insert_id();
        foreach ($workers as $worker) {
            $team_query .=  "('".mysql_insert_id()."', '".clear($worker)."'),";
        }
        $team_query = rtrim($team_query, ",");
        $team_res = mysql_query($team_query) or die(mysql_error());
        return $project_id;
    } else {
        return "false";
    }
}
/* === Добавить проект === */

/* === Обновляем проект === */
function project_edit(){
    $project_id = clear($_POST['project_id']);
    $name = clear($_POST['name']);
    $desc = clear($_POST['desc']);
    $workers = $_POST['workers'];

    $project_query = "UPDATE projects SET name = '$name', description = '$desc' WHERE id = ".$project_id; // обновляем проект
    $project_res = mysql_query($project_query) or die(mysql_error());

    $del_project_workers = "DELETE FROM project_workers WHERE project_id = ".$project_id; // удаляем команду
    $res2 = mysql_query($del_project_workers) or die(mysql_error());


    $team_query = "INSERT INTO project_workers (project_id, worker_id) VALUES "; // добавляем новую команду
    foreach ($workers as $worker) {
        $team_query .=  "('".$project_id."', '".clear($worker)."'),";
    }
    $team_query = rtrim($team_query, ",");
    $team_res = mysql_query($team_query) or die(mysql_error());

    return $project_id;
}
/* === Обновляем проект === */


/* === Удалить проект === */
function project_remove(){
    $project_id = clear($_POST['item_id']);

    $del_projects = "DELETE FROM projects WHERE id = ".$project_id;
    $del_project_workers = "DELETE FROM project_workers WHERE project_id = ".$project_id;
    
    $res1 = mysql_query($del_projects) or die(mysql_error());
    $res2 = mysql_query($del_project_workers) or die(mysql_error());

    return $res;
}
/* === Удалить проект === */

/* === Проект подробнее === */
function get_project_content($project_id){
    $project_sql = "SELECT id, name, description FROM projects WHERE id = $project_id";
    
    $project = mysql_query($project_sql);
    $project_content = array();
    $project_content = mysql_fetch_assoc($project);
    
    return $project_content;
}
/* === Проект подробнее === */

/* === Все задачи === */
function tasks($tasks_id = false){

    $predicat = "";
    if($tasks_id){
        $predicat = "WHERE tasks.id = $tasks_id";
    }
    $query = "SELECT tasks.id, tasks.name, tasks.added, tasks.deadline, tasks.task_anons, tasks.task_desc, tasks.worker AS worker_id, workers.fio AS worker_fio, worker_position.name AS position 
                FROM tasks 
                LEFT JOIN workers ON tasks.worker = workers.id
                LEFT JOIN worker_position ON workers.position = worker_position.id  
                ".$predicat."
                ORDER BY tasks.id DESC";
    $res = mysql_query($query) or die(mysql_error());
    
    $tasks = array();
    while($row = mysql_fetch_assoc($res)){
        $tasks[] = $row;
    }
   

    return $tasks;
}
/* === Все задачи === */

/* === Добавить задачу === */
function task_add(){

    $name = clear($_POST['name']);
    $task_anons = clear($_POST['task_anons']);
    $task_desc = clear($_POST['task_desc']);
    $deadline = clear($_POST['deadline']);
    $worker = clear($_POST['worker']);

    $query = "INSERT INTO tasks (name, task_anons, task_desc, deadline, worker, added) VALUES ('$name', '$task_anons', '$task_desc', '$deadline', '$worker', NOW())";
    $res = mysql_query($query) or die(mysql_error());

    if(mysql_affected_rows() > 0){
        return mysql_insert_id();
    } else {
        return "false";
    }
}
/* === Добавить задачу === */

/* === Удалить задачу === */
function task_remove(){
    $task_id = clear($_POST['item_id']);
    $query = "DELETE FROM tasks WHERE id = ".$task_id;
    $res = mysql_query($query) or die(mysql_error());

    return $res;
}
/* === Удалить задачу === */

/* === Обновляем проект === */
function task_edit(){
    $task_id = clear($_POST['task_id']);
    $name = clear($_POST['name']);
    $task_anons = clear($_POST['task_anons']);
    $task_desc = clear($_POST['task_desc']);
    $deadline = clear($_POST['deadline']);
    $worker = clear($_POST['worker']);

    $query = "UPDATE tasks 
                        SET name = '$name', 
                            task_anons = '$task_anons',
                            task_desc = '$task_desc', 
                            deadline = '$deadline', 
                            worker = '$worker'
                                WHERE id = ".$task_id; // обновляем проект
    $res = mysql_query($query) or die(mysql_error());
    return $task_id;
}
/* === Обновляем проект === */


/* === Удалить сотрудника === */
function worker_remove(){
    $worker_id = clear($_POST['item_id']);

    $del_projects = "DELETE FROM project_workers WHERE worker_id = ".$worker_id; // удаляем сотрудника из всех проектов
    $del_tasks = "DELETE FROM tasks WHERE worker = ".$worker_id; // удаляем задачи сотрудника
    $del_worker = "DELETE FROM workers WHERE id = ".$worker_id; // удаляем сотрудника
    
    $res1 = mysql_query($del_projects) or die(mysql_error());
    $res2 = mysql_query($del_tasks) or die(mysql_error());
    $res3 = mysql_query($del_worker) or die(mysql_error());

    return true;
}
/* === Удалить сотрудника === */

/* === Все Должности === */

function worker_position(){
    $query = "SELECT *
                    FROM worker_position 
                        ORDER BY id ASC";
    $res = mysql_query($query) or die(mysql_error());
    
    $worker_position = array();
    while($row = mysql_fetch_assoc($res)){
        $worker_position[] = $row;
    }
    return $worker_position;
}
/* === Все Должности === */

/* === Добавить сотрудника === */
function worker_add(){

    $fio = clear($_POST['fio']);
    $position = clear($_POST['worker_position']);

    $query = "INSERT INTO workers (fio, position) VALUES ('$fio', '$position')";
    $res = mysql_query($query) or die(mysql_error());

    if(mysql_affected_rows() > 0){
        return mysql_insert_id();
    } else {
        return "false";
    }
}
/* === Добавить сотрудника === */



/* === Обновляем сотрудника === */
function worker_edit(){
    $worker_id = clear($_POST['worker_id']);
    $fio = clear($_POST['fio']);
    $position = clear($_POST['worker_position']);

    $query = "UPDATE workers 
                        SET fio = '$fio', 
                            position = '$position'
                                WHERE id = ".$worker_id; // обновляем проект
    $res = mysql_query($query) or die(mysql_error());
    return $worker_id;
}
/* === Обновляем сотрудника === */

















