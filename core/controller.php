<?php
defined('ZNALEXANDR') or die('Access denied');

session_start();
$login="rbs";
$password="rbs";

 if(!$_SESSION['check']){
  if(isset($_POST['login']) and isset($_POST['pass'])){
      if ($_POST['login']==$login and $_POST['pass']==$password){
         header('location:index.php'); 
         $_SESSION['check'] = true;
          } else die("Uncorently login or password. Please, try again.");
      }
    else  header('location:login.php'); 
}

// подключение модели
require_once MODEL; 

// подключение библиотеки функций
require_once 'functions.php';

if ($_SERVER['HTTP_X_REQUESTED_WITH'] || $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') { // ajax
    
    switch($_GET['ajax']){
       case('project_add'):
         echo project_add();
       break;
       case('project_remove'):
         echo project_remove();
       break;  
       case('project_edit'):
         echo project_edit();
       break; 
       case('task_add'):
         echo task_add();
       break;
       case('task_edit'):
         echo task_edit();
       break;
       case('task_remove'):
         echo task_remove();
       break;
       case('worker_remove'):
         echo worker_remove();
       break;
       case('worker_add'):
         echo worker_add();
       break;
       case('worker_edit'):
         echo worker_edit();
       break;
       default:
         print_r($_POST);
    }

    die();
} 


// получение динамичной части шаблона #content
$view = empty($_GET['view']) ? 'projects' : $_GET['view'];

switch($view){

    case('projects'):
        $projects = projects();
    break;
    case('project_add'):
       $workers = workers();
    break;
    case('project_details'):
        $project_id = abs((int)$_GET['id']);
        $projects = projects($project_id);
        if(!$projects){redirect();}
        $project = $projects[$project_id];
    break;
    case('project_edit'):
        $project_id = abs((int)$_GET['id']);
        $projects = projects($project_id);
        if(!$projects){redirect();}
        $project = $projects[$project_id];
        $workers = workers();
    break;



    case('tasks'):
       $tasks = tasks();
    break;
    case('task_add'):
       $worker_id = abs((int)$_GET['worker_id']);
       $workers = workers();
    break;
    case('task_details'):
        $task_id = abs((int)$_GET['id']);
        $tasks = tasks($task_id);
        if(!$tasks){redirect();}
        $task = $tasks[0];
    break;
    case('task_edit'):
        $task_id = abs((int)$_GET['id']);
        $tasks = tasks($task_id);
        if(!$tasks){redirect();}
        $task = $tasks[0];
        $task['deadline'] = date("Y-m-d", strtotime($task['deadline']));
        $workers = workers();
    break;



    case('workers'):
       $workers = workers(false, "full");
    break;
    case('worker_details'):
       $worker_id = abs((int)$_GET['id']);
       $workers = workers($worker_id, "full");
       if(!$workers){redirect();}
       $worker = $workers[$worker_id]; 
    break;
    case('worker_add'):
       $worker_position = worker_position();
    break;
    case('worker_edit'):
       $worker_id = abs((int)$_GET['id']);
       $workers = workers($worker_id);
       if(!$workers){redirect();}
       $worker = $workers[$worker_id];
       $worker_position = worker_position();
    break;
    


    default:
        $view = 'projects';
        $projects = projects();
}

// подключени вида
require_once VIEW.'index.php';