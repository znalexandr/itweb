<?php defined('ZNALEXANDR') or die('Access denied'); ?>
<ol class="breadcrumb">
  <li><a href="index.php?view=projects">Проекты</a></li>
  <li class="active">Добавить проект</li>
</ol>
<div class="x_panel">
  <div class="x_title">
    <h2>Новый проект</h2>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
	  <!-- start x_content -->
    <form class="form-horizontal form-label-left" id="form">
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="projectname">Имя проекта <span class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="projectname" name="projectname" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="projectdesc">Описание </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <textarea id="projectdesc" name="projectdesc" class="form-control col-md-7 col-xs-12"></textarea>
        </div>
      </div>
      <div class="form-group">
       <label class="control-label col-md-3 col-sm-3 col-xs-12">Выберите сотрудников <span class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <select class="select2_multiple form-control" multiple="multiple" id="workers">
            <?php foreach($workers as $worker): ?>
            <option value="<?php echo $worker["id"]; ?>"><?php echo $worker["position"]." | ".$worker["fio"]; ?></option>
            <?php endforeach; ?>
          </select>
          <small>Зажмите ctrl чтобы выбрать несколько сотрудников</small>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Команда</label>
        <div class="col-md-6 col-sm-6 col-xs-12" id="team">Не выбрано</div>
      </div>
      <div class="ln_solid"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <button class="btn btn-default" type="reset">Сбросить</button>
          <button type="submit" class="btn btn-primary">Добавить проект</button>
        </div>
      </div>
    </form>
    <div id="success" style="display: none;">
      <div class="jumbotron text-center">
        <h3>Успешно</h3>
        <h4>Новый проект добавлен в базу данных</h4>
        <a href="/?view=project_add" class="btn btn-default" id="moreAdd"><i class="fa fa-plus"></i> Добавить еще</a>
        <a href="/?view=project_details&id=" class="btn btn-primary" id="btn-insert-id">Посмотреть проект</a>
      </div>
    </div>

<script type="text/javascript">
$(document).ready(function(){

          $("#form [type=submit]").click(function(e){
            e.preventDefault();
            var isValid = false;
            var name = $('#projectname').val();
            var desc = $('#projectdesc').val();
            var workers = new Array();
            $('#workers option:selected').each(function(){
              workers[workers.length] = $(this).val();
            });

            if(name == ""){
              alert("Введите имя проекта");
            } else if(workers.length < 1){
              alert("Выберите команду");
            } else {
              isValid = true;
            }


            if(isValid){
              $.ajax({
                  url: 'index.php?ajax=project_add',
                  method: 'POST',
                  data: {name: name, desc:desc, workers:workers},
                  success: function(res){
                    if(res != "false"){
                        $("#form").fadeOut(500, function(){
                            var insertId = $("#btn-insert-id").attr('href') + res;
                            $("#btn-insert-id").attr('href', insertId);
                            $("#form")[0].reset();
                            $("#success").fadeIn(500);
                        });
                    }
                  }
              });
            }
          });

          $(document).on("click", "#moreAdd", function(e){
             e.preventDefault();
             $("#success").fadeOut(500, function(){
                $("#form").fadeIn(500);
             });
          });
          function teamSelect(){
              var team = "";
              $("#workers option:selected").each(function(){
                team += '<div>'+$(this).text()+'</div>';
              });
              $('#team').html(team);
          }
           teamSelect();
          $("#workers").change(function(){
            teamSelect();
          });
});
</script>
    <!-- end x_content -->
  </div>
</div>
