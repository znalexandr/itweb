<?php defined('ZNALEXANDR') or die('Access denied'); ?>
<ol class="breadcrumb">
  <li class="active">Проекты</li>
</ol>
<?php //print_arr($projects); ?>
    <?php if($projects): ?>
<div class="x_panel">
  <div class="x_title">
    <h2>Проекты </h2>
    <div class="pull-right">
        <a href="index.php?view=project_add" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить проект</a>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <table class="table table-striped projects">
      <thead>
        <tr>
          <th style="width: 1%">#</th>
          <th style="width: 50%">Имя проекта</th>
          <th>Команда</th>
          <th style="width: 20%">Действия</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach($projects as $project): ?>
        <tr>
          <td><?php echo htmlspecialchars($project["project_id"]); ?></td>
          <td>
            <a href="index.php?view=project_details&id=<?php echo htmlspecialchars($project["project_id"]); ?>"><?php echo htmlspecialchars($project["project_name"]); ?></a>
          </td>
          <td>
            <ul class="list-inline">
              <?php foreach($project["workers"] as $workers): ?>
              <li>
               <a href="index.php?view=worker_details&id=<?php echo htmlspecialchars($workers["worker_id"]); ?>" title="<?php echo htmlspecialchars($workers["worker_fio"])." | ".htmlspecialchars($workers["worker_position"]); ?>"><img src="<?php echo IMAGES; ?>user.png" class="avatar" alt="<?php echo htmlspecialchars($workers["worker_fio"])." | ".htmlspecialchars($workers["worker_position"]); ?>"></a>
              </li>
              <?php endforeach; ?>
            </ul>
          </td>
          <td>
            <a href="index.php?view=project_details&id=<?php echo htmlspecialchars($project["project_id"]); ?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Детали </a>
            <a href="index.php?view=project_edit&id=<?php echo htmlspecialchars($project["project_id"]); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Изменить </a>
            <a href="#" class="btn btn-danger btn-xs" data-remove-item="project" data-item-id="<?php echo htmlspecialchars($project["project_id"]); ?>"><i class="fa fa-trash-o"></i> Удалить </a>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
 
    <!-- end project list -->
  </div>
</div>
 <?php else: ?>
    <div class="jumbotron text-center">
      <h3>В базе нет еще ни одного проекта</h3>
      <h4>Нажмите кнопку "Создать" для того чтобы добавить первый проект</h4>
      <a href="/?view=project_add" class="btn btn-primary"><i class="fa fa-plus"></i> Создать новый проект</a>
    </div>
  <?php endif; ?>
              