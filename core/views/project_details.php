<?php defined('ZNALEXANDR') or die('Access denied'); ?>
<ol class="breadcrumb">
  <li><a href="index.php?view=projects">Проекты</a></li>
  <li class="active">Детали проекта: <?php echo $project["project_name"]; ?></li>
</ol>
<div class="x_panel">
  <div class="x_title">
    <h2>Детали проекта</h2>
    <div class="pull-right">
    <a href="index.php?view=project_edit&id=<?php echo $project["project_id"]; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Изменить </a>
    <a href="#" class="btn btn-danger btn-xs" data-remove-item="project" data-item-id="<?php echo $project["project_id"]; ?>"><i class="fa fa-trash-o"></i> Удалить </a>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
   <!-- start project list -->

   <h4><?php echo $project['project_name']; ?></h4>
   <?php echo $project['project_desc'] == '' ? '<p>Нет описания</p>': '<p>'.$project['project_desc'].'</p>'; ?>
   
   
  <h4>Команда</h4>
    <table class="table table-striped">
      <thead>
        <tr>
          <th style="width: 1%">#</th>
          <th style="width: 70%">ФИО</th>
          <th>Должность</th> 
          <th>Действие</th> 
        </tr>
      </thead>
      <tbody>
        <?php foreach($project['workers'] as $workers): ?>
        <tr>
          <th scope="row"><?php echo $workers['worker_id']; ?></th>
          <td>
            <a href="index.php?view=worker_details.php&id=<?php echo $workers["worker_id"]; ?>" title="<?php echo $workers["worker_fio"]." | ".$workers["worker_position"]; ?>">
              <?php echo $workers["worker_fio"]; ?>
            </a>
          </td>
          <td><?php echo $workers['worker_position']; ?></td>
          <td><a href="index.php?view=worker_details&id=<?php echo $workers['worker_id']; ?>" class="btn btn-xs btn-info">Посмотреть профиль</a></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <!-- end project list -->
  </div>
  </div> 