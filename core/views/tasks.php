<?php defined('ZNALEXANDR') or die('Access denied'); ?>
<ol class="breadcrumb">
  <li class="active">Задачи</li>
</ol>
<?php //print_arr($tasks); ?>
    <?php if($tasks): ?>
<div class="x_panel">
  <div class="x_title">
    <h2>Задачи</h2>
    <div class="pull-right">
        <a href="index.php?view=task_add" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить задачу</a>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
   
  <table class="table table-striped projects">
      <thead>
        <tr>
          <th style="width: 1%">#</th>
          <th>Задача</th>
          <th>Назначена</th>
          <th>DeadLine</th>
          <th>Ответственный</th>
          <th style="width: 20%">Действия</th>
        </tr>
      </thead>
      <tbody>
<?php foreach($tasks as $task): ?>
        <tr>
          <td><?php echo $task['id']; ?></td>
          <td>
            <a href="index.php?view=task_details&id=<?php echo $task['id']; ?>"><?php echo $task['name']; ?></a>
            <br><small><?php echo $task['task_anons']; ?></small>
          </td>
          <td><?php echo dateFun($task['added']); ?></td>
          <td><?php echo dateFun($task['deadline']); ?></td>
          <td>
            <a href="index.php?view=worker_details&id=<?php echo $task['worker_id']; ?>" title="<?php echo $task['position']." | ".$task['worker_fio']; ?>"><?php echo $task['worker_fio']; ?></a>
          </td>
          <td>
            <a href="index.php?view=task_details&id=<?php echo $task['id']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Детали </a>
            <a href="index.php?view=task_edit&id=<?php echo $task['id']; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Изменить </a>
            <a href="#" class="btn btn-danger btn-xs" data-remove-item="task" data-item-id="<?php echo $task['id']; ?>"><i class="fa fa-trash-o"></i> Удалить </a>
          </td>
        </tr>
<?php endforeach; ?>        
      </tbody>
    </table>
    
  </div>
</div>
<?php else: ?>
    <div class="jumbotron text-center">
      <h3>В базе еще нет ни одной задачи</h3>
      <h4>Нажмите кнопку "Создать" для того чтобы создать первую задачу</h4>
      <a href="/?view=task_add" class="btn btn-primary"><i class="fa fa-plus"></i> Создать задачу</a>
    </div>
  <?php endif; ?>      