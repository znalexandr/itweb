<?php defined('ZNALEXANDR') or die('Access denied'); ?>
<ol class="breadcrumb">
  <li><a href="index.php?view=workers">Сотрудники</a></li>
  <li class="active">Добавить сотрудника</li>
</ol>
<div class="x_panel" id="displayform">
  <div class="x_title">
    <h2>Добавить сотрудника</h2>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <?php //print_arr($worker_position); ?>
	  <!-- start x_content -->
      <form id="form" class="form-horizontal form-label-left">

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="worker_fio">ФИО <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="worker_fio" name="worker_fio" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="worker_position">Должность <span class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <select class="select form-control" id="worker_position">
            <option value="0">Выберите</option>
            <?php foreach($worker_position as $position): ?>
              <option value="<?php echo htmlspecialchars($position["id"]); ?>"><?php echo htmlspecialchars($position["name"]); ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="ln_solid"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <button class="btn btn-default" type="reset">Сбросить</button>
          <button type="submit" class="btn btn-primary">Добавить</button>
        </div>
      </div>

    </form>
    <!-- end x_content -->
  </div>
</div>
<div id="success" style="display: none;">
      <div class="jumbotron text-center">
        <h3>Успешно</h3>
        <h4>Новый сотрудник успешно добавлен в базу данных</h4>
        <a href="/?view=worker_add" class="btn btn-default" id="moreAdd"><i class="fa fa-plus"></i> Добавить еще</a>
        <a href="/?view=worker_details&id=" class="btn btn-primary" id="btn-insert-id">Перейти к сотруднику</a>
      </div>
    </div>
 <script type="text/javascript">
$(document).ready(function(){

          $("#form [type=submit]").click(function(e){
            e.preventDefault();
            var isValid = false;

          
            var fio = $('#worker_fio').val();
            var worker_position =  $('#worker_position option:selected').val();
            

            if(fio == ""){
              alert("Введите ФИО");
            } else if(worker_position < 1){
              alert("Выберите должность");
            } else {
              isValid = true;
            }


            if(isValid){
              $.ajax({
                  url: 'index.php?ajax=worker_add',
                  method: 'POST',
                  data: {
                    fio: fio, 
                    worker_position:worker_position
                  },
                  success: function(res){
                    //console.log(res)
                    if(res != "false"){
                        $("#displayform").fadeOut(500, function(){
                            var insertId = $("#btn-insert-id").attr('href') + res;
                            $("#btn-insert-id").attr('href', insertId);
                            $("#form")[0].reset();
                            $("#success").fadeIn(500);
                        });
                    }
                  }
              });
            }
          });

          $(document).on("click", "#moreAdd", function(e){
             e.preventDefault();
             $("#success").fadeOut(500, function(){
                $("#displayform").fadeIn(500);
             });
          });
});
</script>            