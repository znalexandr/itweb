<?php defined('ZNALEXANDR') or die('Access denied'); ?>
<ol class="breadcrumb">
  <li><a href="index.php?view=tasks">Задачи</a></li>
  <li class="active">Детали задачи: <?php echo $task["name"]; ?></li>
</ol>
<?php //print_arr($task); ?>
<div class="x_panel">
  <div class="x_title">
    <h2>Детали задачи</h2>
    <div class="pull-right">
        <a href="index.php?view=task_edit&id=<?php echo $task['id']; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Изменить </a>
        <a href="#" class="btn btn-danger btn-xs" data-remove-item="task" data-item-id="<?php echo $task['id']; ?>"><i class="fa fa-trash-o"></i> Удалить </a>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
	 <!-- start project list -->
    <div>
      <strong>Название:</strong>
      <?php echo $task["name"]; ?>
    </div>
    <div>
      <strong>Краткое описание:</strong>
      <?php echo $project['task_anons'] == '' ? '<p>Нет описания</p>': '<p>'.$project['task_anons'].'</p>'; ?>
    </div> 
    <div>
      <strong>Подробное описание:</strong>
      <?php echo $project['task_desc'] == '' ? '<p>Нет описания</p>': '<p>'.$project['task_desc'].'</p>'; ?>
    </div>  
    <div>
      <strong>Назначена:</strong>
      <?php echo dateFun($task['added']); ?>
    </div>  
    <div>
      <strong>Deadline:</strong>
      <?php echo dateFun($task['deadline']); ?>
    </div>  
    <div>
      <strong>Ответсвенный:</strong>
      <a href="index.php?view=worker_details&id=<?php echo $task['worker_id']; ?>" title="<?php echo $task['position']." | ".$task['worker_fio']; ?>"><?php echo $task['worker_fio']; ?></a>
    </div> 
    <!-- end project list -->
  </div>
</div>