<?php defined('ZNALEXANDR') or die('Access denied'); ?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo TITLE; ?></title>

    <!-- Bootstrap -->
    <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="template/css/custom.css" rel="stylesheet">
    <!-- jQuery -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
    <script src="template/js/jquery.js"></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">