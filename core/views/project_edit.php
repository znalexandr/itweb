<?php defined('ZNALEXANDR') or die('Access denied'); ?>
<ol class="breadcrumb">
  <li><a href="index.php?view=projects">Проекты</a></li>
  <li class="active">Редактировать проект: <?php echo htmlspecialchars($project["project_name"]); ?></li>
</ol>
<?php// print_arr($project); ?>
<div class="x_panel">
  <div class="x_title">
    <h2>Редактировать проект</h2>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
	  <!-- start x_content -->
    <form class="form-horizontal form-label-left" id="form">
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="projectname">Имя проекта <span class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="projectname" name="projectname" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo htmlspecialchars($project["project_name"]); ?>">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="projectdesc">Описание </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <textarea id="projectdesc" name="projectdesc" class="form-control col-md-7 col-xs-12"><?php echo htmlspecialchars($project["project_desc"]); ?></textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Выберите сотрудников <span class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <select class="select2_multiple form-control" multiple="multiple" id="workers">
            <?php 
              foreach($project['workers'] as $project_worker): 
                foreach($workers as $worker): 
            ?>
            <option <?php echo $project_worker["worker_id"] ==  $worker["id"] ? 'selected="selected"' : ''; ?> value="<?php echo htmlspecialchars($worker["id"]); ?>"><?php echo htmlspecialchars($worker["position"])." | ".htmlspecialchars($worker["fio"]); ?></option>
            <?php 
                endforeach; 
              endforeach; 
            ?>
          </select>
          <small>Зажмите ctrl чтобы выбрать несколько сотрудников</small>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Команда</label>
        <div class="col-md-6 col-sm-6 col-xs-12" id="team">Не выбрано</div>
      </div>
      <div class="ln_solid"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <a href="#" class="btn btn-danger" data-remove-item="project" data-item-id="<?php echo htmlspecialchars($project["project_id"]); ?>">Удалить</a>
          <a href="/index.php?view=project_details&id=<?php echo htmlspecialchars($project['project_id']); ?>" class="btn btn-default">Отменить</a>
          <button type="submit" class="btn btn-primary">Сохранить проект</button>
        </div>
      </div>
    </form>
    <div id="success" style="display: none;">
      <div class="jumbotron text-center">
        <h3>Успешно</h3>
        <h4>Изменения внесены в базу данных</h4>
        <a href="/index.php?view=project_edit&id=<?php echo htmlspecialchars($project['project_id']); ?>" class="btn btn-default" id="moreAdd">Редактировать еще</a>
        <a href="/index.php?view=project_details&id=<?php echo htmlspecialchars($project['project_id']); ?>" class="btn btn-primary">Перейти к проекту</a>
      </div>
    </div>

<script type="text/javascript">
$(document).ready(function(){
        
        $("#form [type=submit]").click(function(e){
            e.preventDefault();
            var isValid = false;
            var project_id = <?php echo $project["project_id"]; ?>;
            var name = $('#projectname').val();
            var desc = $('#projectdesc').val();
            var workers = new Array();
            $('#workers option:selected').each(function(){
              workers[workers.length] = $(this).val();
            });

            if(name == ""){
              alert("Введите имя проекта");
            } else if(workers.length < 1){
              alert("Выберите команду");
            } else {
              isValid = true;
            }


            if(isValid){
              $.ajax({
                  url: 'index.php?ajax=project_edit',
                  method: 'POST',
                  data: {project_id:project_id, name: name, desc:desc, workers:workers},
                  success: function(res){
                    console.log(res)
                    if(res != "false"){
                        $("#form").fadeOut(500, function(){
                            $("#success").fadeIn(500);
                        });
                    }
                  }
              });
            }
          });

          $(document).on("click", "#moreAdd", function(e){
             e.preventDefault();
             $("#success").fadeOut(500, function(){
                $("#form").fadeIn(500);
             });
          });

          function teamSelect(){
              var team = "";
              $("#workers option:selected").each(function(){
                team += '<div>'+$(this).text()+'</div>';
              });
              $('#team').html(team);
          }
           teamSelect();
          $("#workers").change(function(){
            teamSelect();
          });
});
</script>
    <!-- end x_content -->
  </div>
</div>
