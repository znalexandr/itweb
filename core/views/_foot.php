<?php defined('ZNALEXANDR') or die('Access denied'); ?>
        <footer>
          <div class="pull-right">
            Александр Зинин &copy; 2017 <a href="mailto:znalexandr@yandex.ru">znalexandr@yandex.ru</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    

    <!-- Bootstrap -->
    <script src="template/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- NProgress -->
    <script src="template/vendors/nprogress/nprogress.js"></script>
     <!-- Custom Theme Scripts -->
    <script src="template/js/custom.min.js"></script>
    <script src="template/js/main.js"></script>
 </body>
</html>