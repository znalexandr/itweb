<?php defined('ZNALEXANDR') or die('Access denied'); ?>
<ol class="breadcrumb">
  <li><a href="index.php?view=tasks">Задачи</a></li>
  <li class="active">Редактировать задачу</li>
</ol>
<div class="x_panel">
  <div class="x_title">
    <h2>Редактировать  задачу</h2>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
	  <!-- start x_content -->
      <form id="form" class="form-horizontal form-label-left">

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="task_name">Название <span class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="task_name" name="task_name" class="form-control col-md-7 col-xs-12" value="<?php echo htmlspecialchars($task["name"]); ?>">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="task_anons">Краткое описание <span class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <textarea id="task_anons" name="task_anons" class="form-control col-md-7 col-xs-12"><?php echo htmlspecialchars($task["task_anons"]); ?></textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="task_desc">Описание</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <textarea id="task_desc" name="task_desc" class="form-control col-md-7 col-xs-12"><?php echo htmlspecialchars($task["task_desc"]); ?></textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="deadline">DeadLine <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="date" id="deadline" name="deadline" class="form-control col-md-7 col-xs-12" value="<?php echo htmlspecialchars($task["deadline"]); ?>">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Ответственный</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <select class="select form-control" id="worker">
            <option value="0">Выберите</option>
            <?php foreach($workers as $worker): ?>
            <option <?php echo $task['worker_id'] ==  $worker["id"] ? 'selected="selected"' : ''; ?> value="<?php echo htmlspecialchars($worker["id"]); ?>"><?php echo htmlspecialchars($worker["position"])." | ".htmlspecialchars($worker["fio"]); ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="ln_solid"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <a href="#" class="btn btn-danger" data-remove-item="task" data-item-id="<?php echo $task["id"]; ?>">Удалить</a>
          <a href="/index.php?view=task_details&id=<?php echo $task['id']; ?>" class="btn btn-default">Отменить</a>
          <button type="submit" class="btn btn-primary">Сохранить проект</button>
        </div>
      </div>
    </form>
    <div id="success" style="display: none;">
      <div class="jumbotron text-center">
        <h3>Успешно</h3>
        <h4>Задача успешно отредактированна</h4>
        <a href="/?view=task_edit&id=<?php echo $task["id"]; ?>" class="btn btn-default" id="moreAdd"><i class="fa fa-plus"></i> Редактировать еще</a>
        <a href="/?view=worker_details&id=" class="btn btn-primary" id="workerid">Перейти к сотруднику</a>
        <a href="/?view=task_details&id=<?php echo $task["id"]; ?>" class="btn btn-primary">Посмотреть задачу</a>
      </div>
    </div>
    <!-- end x_content -->
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

          $("#form [type=submit]").click(function(e){
            e.preventDefault();
            var isValid = false;

            var task_id = <?php echo $task["id"]; ?>;
            var name = $('#task_name').val();
            var task_anons = $('#task_anons').val();
            var task_desc = $('#task_desc').val();
            var deadline = $('#deadline').val();
            var worker =  $('#worker option:selected').val();
            

            if(name == ""){
              alert("Введите имя задачи");
            } else if(deadline < 1){
              alert("Введите deadline");
            } else if(task_anons < 1){
              alert("Введите краткое описание задачи");
            } else if(worker < 1){
              alert("Выберите ответственного");
            } else {
              isValid = true;
            }


            if(isValid){
              $.ajax({
                  url: 'index.php?ajax=task_edit',
                  method: 'POST',
                  data: {
                    task_id:task_id,
                    name: name, 
                    task_anons:task_anons,
                    task_desc:task_desc,
                    deadline:deadline,
                    worker:worker},
                  success: function(res){
                    //console.log(res)
                    if(res != "false"){
                        $("#form").fadeOut(500, function(){
                            $("#workerid").attr('href', ($("#workerid").attr('href')+worker));
                            $("#success").fadeIn(500);
                        });
                    }
                  }
              });
            }
          });

          $(document).on("click", "#moreAdd", function(e){
             e.preventDefault();
             $("#success").fadeOut(500, function(){
                $("#form").fadeIn(500);
             });
          });
});
</script>