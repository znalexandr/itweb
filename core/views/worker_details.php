<?php defined('ZNALEXANDR') or die('Access denied'); ?>
<ol class="breadcrumb">
  <li><a href="index.php?view=workers">Сотрудники</a></li>
  <li class="active">Сотрудник: <?php echo $worker["fio"].', '.$worker["position"]; ?></li>
</ol>
<?php //print_arr($worker); ?>
<div class="x_panel">
  <div class="x_title">
    <h2>Детали сотрудника</h2>
    <div class="pull-right">
      <a href="index.php?view=worker_edit&id=<?php echo $worker['id']; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Изменить </a>
      <a href="#" class="btn btn-danger btn-xs" data-remove-item="worker" data-item-id="<?php echo $worker['id']; ?>"><i class="fa fa-trash-o"></i> Удалить </a>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
	 <!-- start project list -->
   <p>
     <img src="<?php echo IMAGES; ?>user.png" class="avatar" alt="Avatar"  hspace="10">
     <h3><?php echo $worker["fio"].', '.$worker["position"]; ?></h3>
   </p>
   <br><br><br>
   <h4>Ответственный  по задачам: <a href="index.php?view=task_add&worker_id=<?php echo $worker['id']; ?>" class="btn btn-info btn-xs"><i class="fa fa-plus"></i> добавить задачу</a></h4>
     <table class="table table-striped projects">
      <thead>
        <tr>
          <th style="width: 1%">#</th>
          <th>Задача</th>
          <th>Назначена</th>
          <th>DeadLine</th>
          <th style="width: 20%">Действия</th>
        </tr>
      </thead>
      <tbody>
<?php 
if($worker['tasks']):
  foreach($worker['tasks'] as $task): 
?>
        <tr>
          <td><?php echo $task['id']; ?></td>
          <td>
            <a href="index.php?view=task_details&id=<?php echo $task['task_id']; ?>"><?php echo $task['task_name']; ?></a>
            <br><small><?php echo $task['task_anons']; ?></small>
          </td>
          <td><?php echo dateFun($task['task_added']); ?></td>
          <td><?php echo dateFun($task['task_deadline']); ?></td>
          <td>
            <a href="index.php?view=task_details&id=<?php echo $task['task_id']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Детали </a>
            <a href="index.php?view=task_edit&id=<?php echo $task['task_id']; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Изменить </a>
            <a href="#" class="btn btn-danger btn-xs" data-remove-item="task" data-item-id="<?php echo $task['task_id']; ?>"><i class="fa fa-trash-o"></i> Удалить </a>
          </td>
        </tr>
<?php endforeach; else: ?>    
    <tr>
      <td colspan="5" align="center">Нет задач <a href="index.php?view=task_add&worker_id=<?php echo $worker['id']; ?>">добавить задачу</a></td>
    </tr>
<?php endif; ?>        
      </tbody>
    </table>
 <br>
 <h4>Участвует в проектах:</h4>
  <table class="table table-striped projects">
      <thead>
        <tr>
          <th style="width: 1%">#</th>
          <th style="width: 50%">Имя проекта</th>
          <th style="width: 13%">Действия</th>
        </tr>
      </thead>
      <tbody>
<?php 
if($worker['projects']):
  foreach($worker['projects'] as $project):
?>
        <tr>
          <td><?php echo htmlspecialchars($project["projects_id"]); ?></td>
          <td>
            <a href="index.php?view=project_details&id=<?php echo htmlspecialchars($project["projects_id"]); ?>"><?php echo htmlspecialchars($project["projects_name"]); ?></a>
          </td>
          <td>
            <a href="index.php?view=project_details&id=<?php echo htmlspecialchars($project["projects_id"]); ?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Детали </a>
            <a href="index.php?view=project_edit&id=<?php echo htmlspecialchars($project["projects_id"]); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Изменить </a>
            <a href="#" class="btn btn-danger btn-xs" data-remove-item="project" data-item-id="<?php echo htmlspecialchars($project["projects_id"]); ?>"><i class="fa fa-trash-o"></i> Удалить </a>
          </td>
        </tr>
        <?php endforeach; else: ?>
        <tr>
          <td colspan="5" align="center">Нет Проектов</td>
        </tr>
      <?php endif; ?>
      </tbody>
    </table>

  </div>
</div>
             