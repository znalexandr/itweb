<?php defined('ZNALEXANDR') or die('Access denied'); ?>
<ol class="breadcrumb">
  <li class="active">Сотрудники</li>
</ol>
<?php //print_arr($workers); ?>
    <?php if($workers): ?>
<div class="x_panel">
  <div class="x_title">
    <h2>Сотрудники</h2>
    <div class="pull-right">
        <a href="index.php?view=worker_add" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить сотрудника</a>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <table class="table table-striped projects">
      <thead>
        <tr>
          <th style="width: 1%">#</th>
          <th style="width: 1%"></th>
          <th>Сотрудник</th>
          <th style="width: 10%">Участвует</th>
          <th style="width: 10%">Делает</th>
          <th style="width: 20%">Действия</th>
        </tr>
      </thead>
      <tbody>
      <?php 
            foreach($workers as $worker):
                 $project_count = htmlspecialchars($worker['projects_count']);
                 $project_count = ($project_count) ? plural_text($project_count,  $project_count.'-м проекте', $project_count.'-х проектах', $project_count.'-ти проектах') : 'Нет проектов'; 

                 $tasks_count = htmlspecialchars($worker['tasks_count']);
                 $tasks_count = ($tasks_count) ? plural_text($tasks_count,  $tasks_count.'-у задачу', $tasks_count.'-е задачи', $tasks_count.' задач') : 'свободен';  
         ?>
        <tr>
          <td><?php echo $worker['id']; ?></td>
          <td>
            <a href="index.php?view=worker_details&id=<?php echo htmlspecialchars($worker['id']); ?>"><img src="<?php echo IMAGES; ?>user.png" class="avatar" alt="Avatar"></a>
          </td>
          <td>
            <a href="index.php?view=worker_details&id=<?php echo htmlspecialchars($worker['id']); ?>"><?php echo htmlspecialchars($worker['fio']); ?></a><br>
            <small><?php echo htmlspecialchars($worker['position']); ?></small>
          </td>
          <td><?php echo $project_count; ?></td>
          <td><?php echo $tasks_count; ?></td>
          <td>
            <a href="index.php?view=worker_details&id=<?php echo htmlspecialchars($worker['id']); ?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Детали </a>
            <a href="index.php?view=worker_edit&id=<?php echo htmlspecialchars($worker['id']); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Изменить </a>
            <a href="#" class="btn btn-danger btn-xs" data-remove-item="worker" data-item-id="<?php echo $worker['id']; ?>"><i class="fa fa-trash-o"></i> Удалить </a>
          </td>
        </tr>
    <?php endforeach; ?>       
      </tbody>
    </table>
  </div>
</div>     
<?php else: ?>
  <div class="jumbotron text-center">
    <h3>В базе еще нет ни одной сотрудника</h3>
    <h4>Нажмите кнопку "добавить" для того чтобы добавить нового сотрудника</h4>
    <a href="/?view=worker_add" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить сотрудника</a>
  </div>
<?php endif; ?>   